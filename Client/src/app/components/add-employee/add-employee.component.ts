import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  employee = {
    name: '',
    salary: '',
    age: '',
    published: false,
  };
  submitted = false;
  valid = true;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
  }
// Create an Employee
  saveEmployee(): void {
    const data = {
      name: this.employee.name,
      salary: this.employee.salary,
      age: this.employee.age
    };

    if (data.age == null || data.salary == null || data.age === '' || data.salary === '' || data.name === ''){
      this.valid = false ;
    }
    else{
      this.employeeService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
          this.valid = true;
        },
        error => {
          console.log(error);
        });
    }
  }

  newEmployee(): void {
    this.submitted = false;
    this.employee = {
      name: '',
      salary: '',
      age: '',
      published: false
    };
  }
}
