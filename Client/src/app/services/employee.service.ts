import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const baseUrl = 'http://dummy.restapiexample.com/api/v1';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  constructor(private http: HttpClient) { }
  // Get all Employee data
  getAll() {
    return this.http.get(`${baseUrl}/employees`);
  }
// Get a single employee data
  getEmployee(id) {
    return this.http.get(`${baseUrl}/employees/${id}`);
  }
// Create new record in database
  create(data) {
    return this.http.post(`${baseUrl}/create`, data);
  }
// Update an employee record
  update(id, data) {
    return this.http.put(`${baseUrl}/update/${id}`, data);
  }
// Delete an employee record
  delete(id) {
    return this.http.delete(`${baseUrl}/delete/${id}`);
  }
}
